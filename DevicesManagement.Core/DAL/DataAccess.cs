﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevicesManagement.Core.Models;
using DevicesManagement.Core.Helpers;

namespace DevicesManagement.Core.DAL
{
    public class DataAccess
    {
        public static void InitializeDatabase()
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                String tableCommand = "create table if not exists users (" +
                    "userid integer not null primary key AUTOINCREMENT, " +
                    "user_username nvarchar(100),user_password nvarchar(100)); " +
                    "create table if not exists devicetypes(" +
                    "devicetype_id integer not null primary key AUTOINCREMENT, " +
                    "devicetype_name nvarchar(100)); " +
                    "create table if not exists devices(" +
                    "device_id integer not null primary key AUTOINCREMENT, " +
                    "device_name nvarchar(100) not null, " +
                    "devicetype_id integer not null, " +
                    "devicestatuses_id integer not null, " +
                    "device_image nvarchar(100), " +
                    "foreign key(devicetype_id) REFERENCES devicetypes (devicetype_id)); " +
                    "create table if not exists employees(" +
                    "employee_id integer not null primary key AUTOINCREMENT, " +
                    "employee_name nvarchar(100), " +
                    "employe_numberphone nvarchar(100), " +
                    "employee_email nvarchar(100), " +
                    "employee_avatar nvarchar(100)); " +
                    "create table if not exists employees_devices(" +
                    "device_id integer primary key not null, " +
                    "employee_id integer not null, " +
                    "date_hire Date not null, " +
                    "foreign key(device_id) references devices(device_id));";
                //String tableCommand2 = "CREATE TABLE IF NOT " +
                //    "EXISTS MyTable (Primary_Key INTEGER PRIMARY KEY, " +
                //    "Text_Entry NVARCHAR(2048) NULL)"; 
                SqliteCommand createTable = new SqliteCommand(tableCommand, db);
                createTable.ExecuteReader();
               //Migration();
            }
        }

        public static void Migration()
        {
            AddDeviceType(new DeviceType() { devicetype_name = "Monitor" });
            AddDeviceType(new DeviceType() { devicetype_name = "CPU" });
            AddDeviceType(new DeviceType() { devicetype_name = "Keyboard" });
            AddDeviceType(new DeviceType() { devicetype_name = "Mouse" });
            AddDevice(new Device() { device_name = "Dell 23 inch", device_image = "/Assets/Devices/monitor1.jpg", devicestatuses_id = Const.DEVICESTATUS.READY, devicetype_id = 1 });
            AddDevice(new Device() { device_name = "LG 23 inch", device_image = "/Assets/Devices/monitor2.jpg", devicestatuses_id = Const.DEVICESTATUS.READY, devicetype_id = 1 });
        }

        public static void AddEmployee(Employee employee)
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "INSERT INTO employees VALUES (NULL, @employee_name, @employe_numberphone, @employee_email, @employee_avatar);";
                insertCommand.Parameters.AddWithValue("@employee_name", employee.employee_name);
                insertCommand.Parameters.AddWithValue("@employe_numberphone", employee.employe_numberphone);
                insertCommand.Parameters.AddWithValue("@employee_email", employee.employee_email);
                insertCommand.Parameters.AddWithValue("@employee_avatar", employee.employee_avatar);
                insertCommand.ExecuteReader();

                db.Close();
            }

        }
        public static ObservableCollection<Employee> GetEmployees()
        {
            ObservableCollection<Employee> employees = new ObservableCollection<Employee>();

            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand selectCommand = new SqliteCommand
                    ("SELECT * from employees", db);

                SqliteDataReader query = selectCommand.ExecuteReader();

                while (query.Read())
                {
                    Employee employee = new Employee();
                    employee.employee_id = query.GetInt32(0);
                    employee.employee_name = query.GetString(1);
                    employee.employe_numberphone = query.GetString(2);
                    employee.employee_email = query.GetString(3);
                    employee.employee_avatar = query.GetString(4);
                    employees.Add(employee);
                }

                db.Close();
            }

            return employees;
        }

        public static void EditEmployee(Employee employee)
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "UPDATE employees SET employee_name = @employee_name, employe_numberphone = @employe_numberphone, employee_email = @employee_email, employee_avatar = @employee_avatar WHERE employee_id = @employee_id;";
                insertCommand.Parameters.AddWithValue("@employee_name", employee.employee_name);
                insertCommand.Parameters.AddWithValue("@employe_numberphone", employee.employe_numberphone);
                insertCommand.Parameters.AddWithValue("@employee_email", employee.employee_email);
                insertCommand.Parameters.AddWithValue("@employee_avatar", employee.employee_avatar);
                insertCommand.Parameters.AddWithValue("@employee_id", employee.employee_id);
                insertCommand.ExecuteReader();

                db.Close();
            }

        }

        public static void DeleteEmployee(Employee employee)
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "DELETE FROM employees WHERE employee_id = @employee_id;";
                insertCommand.Parameters.AddWithValue("@employee_id", employee.employee_id);
                insertCommand.ExecuteReader();

                db.Close();
            }

        }

        public static void AddDeviceType(DeviceType deviceType)
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();
                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;
                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "INSERT INTO devicetypes VALUES (NULL, @devicetype_name);";
                insertCommand.Parameters.AddWithValue("@devicetype_name", deviceType.devicetype_name);
                insertCommand.ExecuteReader();

                db.Close();
            }

        }

        public static ObservableCollection<DeviceType> GetDeviceTypes()
        {
            ObservableCollection<DeviceType> deviceTypes = new ObservableCollection<DeviceType>();

            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand selectCommand = new SqliteCommand
                    ("SELECT * from devicetypes", db);

                SqliteDataReader query = selectCommand.ExecuteReader();

                while (query.Read())
                {
                    DeviceType deviceType = new DeviceType();
                    deviceType.devicetype_id = query.GetInt32(0);
                    deviceType.devicetype_name = query.GetString(1);
                    deviceTypes.Add(deviceType);
                }

                db.Close();
            }

            return deviceTypes;
        }

        public static void EditDeviceType(DeviceType deviceType)
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "UPDATE devicetypes SET devicetype_name = @devicetype_name WHERE devicetype_id = @devicetype_id;";
                insertCommand.Parameters.AddWithValue("@devicetype_name", deviceType.devicetype_name);
                insertCommand.Parameters.AddWithValue("@devicetype_id", deviceType.devicetype_id);
                insertCommand.ExecuteReader();

                db.Close();
            }

        }

        public static void DeleteDeviceType(DeviceType deviceType)
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "DELETE FROM devicetypes WHERE devicetype_id = @devicetype_id;";
                insertCommand.Parameters.AddWithValue("@devicetype_id", deviceType.devicetype_id);
                insertCommand.ExecuteReader();

                db.Close();
            }

        }



        public static void AddDevice(Device device)
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "INSERT INTO devices VALUES (NULL, @device_name, @devicetype_id, @devicestatuses_id, @device_image);";
                insertCommand.Parameters.AddWithValue("@device_name", device.device_name);
                insertCommand.Parameters.AddWithValue("@devicetype_id", device.devicetype_id);
                insertCommand.Parameters.AddWithValue("@devicestatuses_id", (int)device.devicestatuses_id);
                insertCommand.Parameters.AddWithValue("@device_image", device.device_image);
                insertCommand.ExecuteReader();

                db.Close();
            }

        }

        public static ObservableCollection<Device> GetDevices()
        {
            ObservableCollection<Device> devices = new ObservableCollection<Device>();

            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand selectCommand = new SqliteCommand
                    ("SELECT * from devices", db);

                SqliteDataReader query = selectCommand.ExecuteReader();

                while (query.Read())
                {
                    Device device = new Device();
                    device.device_id = query.GetInt32(0);
                    device.device_name = query.GetString(1);
                    device.devicetype_id = query.GetInt32(2);
                    device.devicestatuses_id = (Const.DEVICESTATUS)query.GetInt32(3);
                    device.device_image = query.GetString(4);
                    devices.Add(device);
                }

                db.Close();
            }

            return devices;
        }

        public static void EditDevice(Device device)
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "UPDATE devices SET device_name = @device_name, devicetype_id = @devicetype_id, devicestatuses_id = @devicestatuses_id, device_image = @device_image WHERE device_id = @device_id;";
                insertCommand.Parameters.AddWithValue("@device_name", device.device_name);
                insertCommand.Parameters.AddWithValue("@devicetype_id", device.devicetype_id);
                insertCommand.Parameters.AddWithValue("@devicestatuses_id", device.devicestatuses_id);
                insertCommand.Parameters.AddWithValue("@device_image", device.device_image);
                insertCommand.Parameters.AddWithValue("@device_id", device.device_id);
                insertCommand.ExecuteReader();

                db.Close();
            }

        }

        public static void DeleteDevice(int id)
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=DevicesSQLite.db"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "DELETE FROM devices WHERE device_id = @device_id;";
                insertCommand.Parameters.AddWithValue("@device_id", id);
                insertCommand.ExecuteReader();

                db.Close();
            }

        }







    }
}

﻿using DevicesManagement.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace DevicesManagement.Core.Models
{
    public class Device
    {
        public int device_id {get;set;}
        public string device_name {get;set;}
        public int devicetype_id {get;set;}
        public string device_image {get;set;}
        public Const.DEVICESTATUS devicestatuses_id {get;set;}
    }
}

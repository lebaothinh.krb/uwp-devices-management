﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevicesManagement.Core.Models
{
    public class DeviceType
    {
        public int devicetype_id { get; set; }
        public string devicetype_name { get; set; }
    }
}

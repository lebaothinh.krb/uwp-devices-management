﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevicesManagement.Core.Models
{
    class Employees_Devices
    {
        public int _employeeId { get; set; }
        public int _deviceId {get;set;}
        public DateTime _dateHire { get; set; }
    }
}

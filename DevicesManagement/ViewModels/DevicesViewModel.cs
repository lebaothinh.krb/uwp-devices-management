﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using DevicesManagement.Core.Models;
using Windows.UI.Popups;
using System.Windows.Input;
using DevicesManagement.Services;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage;
using Windows.ApplicationModel;
using System.ComponentModel;

namespace DevicesManagement.ViewModels
{
    public class DevicesViewModel: INotifyPropertyChanged
    {
        DeviceService deviceService = new DeviceService();
        DevicesTypeService devicesTypeService = new DevicesTypeService();
        public DevicesViewModel()
        {
            LoadDevices();
            LoadDevicesType();
        }
        ObservableCollection<DeviceViewModel> devices = new ObservableCollection<DeviceViewModel>();
        public ObservableCollection<DeviceViewModel> Devices
        {
            get { return devices; }
            set
            {
                devices = value;
                //PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Devices)));
            }
        }

        ObservableCollection<DeviceTypeViewModel> deviceTypes = new ObservableCollection<DeviceTypeViewModel>();
        public ObservableCollection<DeviceTypeViewModel> DeviceTypes
        {
            get { return deviceTypes; }
            set
            {
                deviceTypes = value;
            }
        }

        private DeviceViewModel addPanelDevice = new DeviceViewModel() { DeviceName = "", Image = "", Status = Core.Helpers.Const.DEVICESTATUS.READY, Type = null};
        public DeviceViewModel AddPanelDevice
        {
            get { return addPanelDevice; }
            set
            {
                addPanelDevice = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(AddPanelDevice)));
            }
        }

        private bool allChecked = false;
        public bool AllChecked
        {
            get { return allChecked; }
            set
            {
                allChecked = value;
                Devices.ToList().ForEach(d => { d.IsChecked = allChecked; });
            }
        }

        private string isEnabledAddPanel = "Collapsed";

        public event PropertyChangedEventHandler PropertyChanged;

        public string IsEnabledAddPanel
        {
            get { return isEnabledAddPanel; }
            set
            {
                isEnabledAddPanel = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsEnabledAddPanel)));
            }
        }
        private string isUpdateAddPanel = "Collapsed";
        public string IsEnabledUpdatePanel
        {
            get { return isUpdateAddPanel; }
            set
            {
                isUpdateAddPanel = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsEnabledUpdatePanel)));
            }
        }

        public void LoadDevicesType()
        {
            DeviceTypes = new ObservableCollection<DeviceTypeViewModel>();
            var deviceTypesDb = devicesTypeService.GetAllDeviceTypes();
            foreach (var type in deviceTypesDb)
            {
                this.DeviceTypes.Add(new DeviceTypeViewModel() { TypeId = type.devicetype_id, TypeName = type.devicetype_name});
            }
        }

        public void LoadDevices()
        {
            Devices.Clear();
            var devicesDb= deviceService.GetAllDevices();
            foreach(var device in devicesDb)
            {
                this.Devices.Add(new DeviceViewModel() { DeviceId = device.device_id, device_name = device.device_name, Image = device.device_image, Status = device.devicestatuses_id, Type = devicesTypeService.GetType(device.devicetype_id) });
            }
        }

        public ICommand btnDelete_Clicked
        {
            get { return new DelegateCommand(OnDeleteAsync); }
        }

        public ICommand btnUpdate_Clicked
        {
            get { return new DelegateCommand(OnUpdateAsync); }
        }

        public ICommand btnOpenUpdate_Clicked
        {
            get { return new DelegateCommand(OpenUpdateAsync); }
        }

        public ICommand btnAdd_Clicked
        {
            get { return new DelegateCommand(OnAddAsync); }
        }

        public ICommand btnOpenAdd_Clicked
        {
            get { return new DelegateCommand(OpenAdding); }
        }

        public ICommand btnUploadImage_Clicked
        {
            get { return new DelegateCommand(UploadImageAsync); }
        }

        public ICommand btnCancel_Clicked
        {
            get { return new DelegateCommand(HidePanel); }
        }

        public ICommand btnRefresh_Clicked
        {
            get { return new DelegateCommand(LoadDevices); }
        }

        public void HidePanel()
        {
            IsEnabledAddPanel = "Collapsed";
            IsEnabledUpdatePanel = "Collapsed";
        }

        public void OpenAdding()
        {
            ResetDeviceModel();
            IsEnabledAddPanel = IsEnabledAddPanel == "Collapsed" ? "Visible" : "Collapsed";
            IsEnabledUpdatePanel = "Collapsed";
        }
        public async void OpenUpdateAsync()
        {
            List<int> ilist = Devices.Where(d => d.IsChecked == true).Select(d => d.DeviceId).ToList();
            if (ilist.Count == 1)
            {
                var mode = Devices.Where(d => d.IsChecked == true).FirstOrDefault();
                this.AddPanelDevice.DeviceId = mode.DeviceId;
                this.AddPanelDevice.DeviceName = mode.DeviceName;
                this.AddPanelDevice.Image = mode.Image;
                this.AddPanelDevice.IsChecked = false;
                this.AddPanelDevice.Status = mode.Status;
                this.AddPanelDevice.Type = mode.Type;
                IsEnabledUpdatePanel = IsEnabledUpdatePanel == "Collapsed" ? "Visible" : "Collapsed";
                IsEnabledAddPanel = "Collapsed";
            }
            else
            {
                var dialog = new MessageDialog("Please choose one!");
                await dialog.ShowAsync();
            }
        }

        public async void OnDeleteAsync()
        {
            List<int> ilist = Devices.Where(d => d.IsChecked == true).Select(d => d.DeviceId).ToList();
            //foreach(int i in ilist)
            //{
            //    Devices.Remove(Devices.Where(d => d.DeviceId == i).FirstOrDefault());
            //}
            deviceService.DeleteDevices(ilist);
            LoadDevices();
            var dialog = new MessageDialog("Delete Successfully!");
            await dialog.ShowAsync();
        }

        public async void UploadImageAsync()
        {
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation =
                Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");
            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
            var storageFolder = await Package.Current.InstalledLocation.GetFolderAsync("Assets");
            try
            {
                await file.CopyAsync(storageFolder);
            }
            catch { }
            if (file != null)
            {
                AddPanelDevice.Image = "/Assets/" + file.Name;
                //using (Windows.Storage.Streams.IRandomAccessStream fileStream =
                //    await file.OpenAsync(Windows.Storage.FileAccessMode.Read))
                //{
                //    BitmapImage bitmapImage = new BitmapImage();
                //    bitmapImage.SetSource(fileStream);
                //    AddPanelDevice.Image = bitmapImage.UriSource.ToString();
                //}
            }
        }

        public async void OnUpdateAsync()
        {
            deviceService.UpdateDevice(this.addPanelDevice);
            LoadDevices();
            var dialog = new MessageDialog("Update Successfully!");
            await dialog.ShowAsync();
        }

        public async void OnAddAsync()
        {
            deviceService.AddDevice(this.addPanelDevice);
            LoadDevices();
            var dialog = new MessageDialog("Add Successfully!");
            await dialog.ShowAsync();
        }

        public void ResetDeviceModel()
        {
            addPanelDevice.DeviceName = "";
            addPanelDevice.Image = "";
            addPanelDevice.Status = Core.Helpers.Const.DEVICESTATUS.READY;
            addPanelDevice.Type = devicesTypeService.GetDefaultType();
        }
    }
}

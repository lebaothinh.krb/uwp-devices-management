﻿using DevicesManagement.Core.Helpers;
using DevicesManagement.Core.Models;
using DevicesManagement.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DevicesManagement.ViewModels
{
    public class DeviceViewModel: Device, INotifyPropertyChanged
    {
        DevicesTypeService devicesTypeService = new DevicesTypeService();
        public DeviceViewModel()
        {

        }
        public int DeviceId
        {
            get { return device_id; }
            set
            {
                device_id = value;
            }
        }

        public string DeviceName
        {
            get { return device_name; }
            set
            {
                device_name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(DeviceName)));
            }
        }

        public DeviceType Type 
        {
            get { return new DeviceType { devicetype_id = devicetype_id, devicetype_name = devicesTypeService.GetTypeName(devicetype_id)}; }
            set
            {
                if (value != null)
                devicetype_id = value.devicetype_id;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Type)));
            }
        }

        public string Image
        {
            get { return device_image; }
            set
            {
                device_image = String.IsNullOrEmpty(value) ? "/Assets/default.png" : value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Image)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ImageName)));
            }
        }

        public string ImageName
        {
            get
            {
                if (!String.IsNullOrEmpty(Image))
                {
                    var imagenames = device_image.Split('/').ToList();
                    return imagenames[imagenames.Count - 1];
                }
                return "No File Choosen";
            }
        }

        public Const.DEVICESTATUS Status
        {
            get { return devicestatuses_id; }
            set
            {
                devicestatuses_id = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Status)));
            }
        }
        private bool _isChecked = false;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsChecked)));
            }
        }

    }
}

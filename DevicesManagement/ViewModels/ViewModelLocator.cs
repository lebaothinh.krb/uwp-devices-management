﻿using System;

using DevicesManagement.Services;
using DevicesManagement.Views;

using GalaSoft.MvvmLight.Ioc;

namespace DevicesManagement.ViewModels
{
    [Windows.UI.Xaml.Data.Bindable]
    public class ViewModelLocator
    {
        private static ViewModelLocator _current;

        public static ViewModelLocator Current => _current ?? (_current = new ViewModelLocator());

        private ViewModelLocator()
        {
            SimpleIoc.Default.Register(() => new NavigationServiceEx());
            SimpleIoc.Default.Register<ShellViewModel>();
            Register<MainViewModel, MainPage>();
            Register<HireViewModel, HirePage>();
            Register<EmployeesViewModel, EmployeesPage>();
            Register<DevicesViewModel, DevicesPage>();
            Register<CategoriesViewModel, CategoriesPage>();
        }

        public MainViewModel MainViewModel => SimpleIoc.Default.GetInstance<MainViewModel>();
        public HireViewModel HireViewModel => SimpleIoc.Default.GetInstance<HireViewModel>();
        public EmployeesViewModel EmployeesViewModel => SimpleIoc.Default.GetInstance<EmployeesViewModel>();
        public DevicesViewModel DevicesViewModel => SimpleIoc.Default.GetInstance<DevicesViewModel>();
        public CategoriesViewModel CategoriesViewModel => SimpleIoc.Default.GetInstance<CategoriesViewModel>();
        public ShellViewModel ShellViewModel => SimpleIoc.Default.GetInstance<ShellViewModel>();

        public NavigationServiceEx NavigationService => SimpleIoc.Default.GetInstance<NavigationServiceEx>();

        public void Register<VM, V>()
            where VM : class
        {
            SimpleIoc.Default.Register<VM>();

            NavigationService.Configure(typeof(VM).FullName, typeof(V));
        }
    }
}

﻿using DevicesManagement.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevicesManagement.ViewModels
{
    public class DeviceTypeViewModel: DeviceType
    {
        public int TypeId
        {
            get { return devicetype_id; }
            set
            {
                devicetype_id = value;
            }
        }
        public string TypeName
        {
            get { return devicetype_name; }
            set
            {
                devicetype_name = value;
            }
        }
    }
}

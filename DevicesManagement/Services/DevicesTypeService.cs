﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevicesManagement.Core.Models;

namespace DevicesManagement.Services
{
    public class DevicesTypeService
    {
        private ObservableCollection<DeviceType> types = new ObservableCollection<DeviceType>()
        {
            new DeviceType(){devicetype_id = 1, devicetype_name = "Monitor"},
            new DeviceType(){devicetype_id = 2, devicetype_name = "CPU"},
            new DeviceType(){devicetype_id = 3, devicetype_name = "Keyboard"},
            new DeviceType(){devicetype_id = 4, devicetype_name = "Mouse"},
        };
        public ObservableCollection<DeviceType> GetAllDeviceTypes()
        {
            return types;
        }
        public string GetTypeName(int id)
        {
            return types.Where(t => t.devicetype_id == id).Select(t => t.devicetype_name).FirstOrDefault();
        }
        public DeviceType GetDefaultType()
        {
            return types.FirstOrDefault();
        }
        public DeviceType GetType(int id)
        {
            return types.Where(t => t.devicetype_id == id).FirstOrDefault();
        }
    }
}

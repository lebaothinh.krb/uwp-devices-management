﻿using DevicesManagement.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.ObjectModel;
using DevicesManagement.Core.DAL;

namespace DevicesManagement.Services
{
    public class DeviceService
    {
        //ObservableCollection<Device> devices = new ObservableCollection<Device>()
        //    {
        //        new Device(){ device_id = 1, device_name = "Monitor Dell 23 inch", devicestatuses_id = Core.Helpers.Const.DEVICESTATUS.READY, devicetype_id = 1, device_image = "/Assets/Devices/monitor1.jpg"},
        //        new Device(){ device_id = 2, device_name = "Monitor LG 23 inch", devicestatuses_id = Core.Helpers.Const.DEVICESTATUS.READY, devicetype_id = 1, device_image = "/Assets/Devices/monitor2.jpg"},
        //    };
        public ObservableCollection<Device> GetAllDevices()
        {
            return DataAccess.GetDevices();
        }

        public void AddDevice(Device device)
        {
            DataAccess.AddDevice(device);
        }

        public void DeleteDevices(List<int> ilist)
        {
            foreach (int index in ilist)
            {
                DataAccess.DeleteDevice(index);
            }
        }

        public void UpdateDevice(Device device)
        {
            DataAccess.EditDevice(device);
        }
    }
}

﻿using System;

using DevicesManagement.ViewModels;

using Windows.UI.Xaml.Controls;

namespace DevicesManagement.Views
{
    public sealed partial class MainPage : Page
    {
        private MainViewModel ViewModel
        {
            get { return ViewModelLocator.Current.MainViewModel; }
        }

        public MainPage()
        {
            InitializeComponent();
        }
    }
}
